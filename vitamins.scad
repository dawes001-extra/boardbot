include </usr/share/openscad/libraries/MCAD/boxes.scad>
include </usr/share/openscad/libraries/MCAD/nuts_and_bolts.scad>

module idler_wheel(solid=false) {
  color("pink") translate([0,0,4.25]) {
  difference() { union() {
      translate([0,0,0])cylinder(r=6,h=8.5,center=true);
      translate([0,0,3.75])cylinder(r=9,h=1,center=true);
      translate([0,0,-3.75])cylinder(r=9,h=1,center=true);
    }
    if (solid == false) {
      cylinder(r=1.5,h=20,center=true);
    }
  }}
}

module runner_wheel(solid=false) {
  color("darkgreen") translate([0,0,-5]) {
  difference() { union() {
      translate([0,0,1.25])cylinder(r1=9.5,r2=12.2,h=2.5,center=true);
      translate([0,0,3.75])cylinder(r2=9.5,r1=12.2,h=2.5,center=true);
      translate([0,0,-1.25])cylinder(r2=9.5,r1=12.2,h=2.5,center=true);
      translate([0,0,-3.75])cylinder(r1=9.5,r2=12.2,h=2.5,center=true);
    }
    if (solid == false) {
      cylinder(r=2.5,h=20,center=true);
    }
  }}
}

module eccentric_nut(solid=false) {
  color("red") difference() {union() {
    translate([0,0,1.25])cylinder(r=7.1/2,h=2.5,center=true);
    translate([0,0,-6.1/2])cylinder(r=10.9/2,h=6.1,center=true);
    }
    if (solid == false) {
        translate([0.6,0,0])cylinder(r=2.5,h=20,center=true);
    }
  }
}

module drive_wheel(solid=false) {
  color("lime") translate([0,0,9])difference() {union() {
    translate([0,0,0])cylinder(r=10,h=18,center=true);
    translate([0,0,18/2-1.5/2])cylinder(r=21.1,h=1.5,center=true);
    translate([0,0,3.5])cylinder(r=37.7/2,h=11,center=true);
    translate([0,0,18/2-11+1.5/2])cylinder(r=21.1,h=1.5,center=true);
    }
    if (solid == false) {
      cylinder(r=2.5,h=30,center=true);
    }
  }
}

module base_rail(length=2000) {
  color("grey") rotate([45,0,0]) union() {
    translate([0,-10,-1])cube([length,20,2],center=true);
    translate([0,-1,-10])cube([length,2,20],center=true);
    }
}

module belt(from,to,angle=0) {
  length = norm([from[0]-to[0],from[1]-to[1],from[2]-to[2]]);
  phi = acos((to[2]-from[2])/length);
  theta = atan2(to[1]-from[1],to[0]-from[0]);
  translate(from)
  rotate([0, phi, theta])
    rotate([0,0,angle]) 
      color("black")
        translate([-1,-3,0]) cube([2,6,length]);
}

module board() {
  color("white") translate([0,-9,0]) difference() { union (){
  rotate([90,0,0])roundedBox([1494,1195,18],7,sidesonly=true);
    for (x = [1,-1]) { for (z = [1,-1]) {
        translate([(741-4*sqrt(2))*x,9,(591.5-4*sqrt(2))*z])rotate([0,-45*x*z,0])cube([10,4.5,5],center=true);
      }}
    }
    for (x = [1,-1]) {
      for (z = [1,-1]) {
        translate([741*x,6.5,591.5*z])rotate([90,0,0])cylinder(r=3,h=5,center=true);
        translate([741*x,0,591.5*z])rotate([90,0,0])cylinder(r=2,h=20,center=true);
      }  
    }
  }
}

module stepper() {
  color("purple") translate([0,0,-26.4]) difference() {union (){
    translate([0,0,0]) cube([42.2,42.2,47.2],center=true);
    translate([0,0,25-47.2/2]) cylinder(r=11.05,h=50,center=true);
    translate([0,0,71.5/2-47.2/2]) cylinder(r=2.5,h=71.5,center=true);
  }
  for (x=[1,-1]) {for (y=[1,-1]) {
    translate([15.5*x,15.5*y,47.2/2]) rotate([180,0,0]) boltHole(3,6);
  }}}
}

module biro_spring(compression=0) {
  cylinder(r=4.15/2,h=10+15*(1-compression),center=true);
}

module tension_spring(extension=0) {
  length=11.2*(extension+1);
  translate([0,0,length/2+4.2/2]) rotate([90,0,0]) difference() {
  cylinder(r=4.2/2,h=0.5,center=true);
  cylinder(r=3.2/2,h=1,center=true);
  }
  translate([0,0,-length/2-4.2/2]) rotate([90,0,0]) difference() {
  cylinder(r=4.2/2,h=0.5,center=true);
  cylinder(r=3.2/2,h=1,center=true);
  }
  cylinder(r=4.2/2,h=length,center=true);
}

module staedler_pen() {
  translate([0,0,0])cylinder(r1=0,r2=2,h=2);
  translate([0,0,2])cylinder(r1=2,r2=2,h=4);
  translate([0,0,6])cylinder(r1=6.8/2,r2=4,h=2);
  translate([0,0,8])cylinder(r1=4,r2=4,h=4);
  translate([0,0,12])cylinder(r1=4,r2=11/2,h=4);
  translate([0,0,16])cylinder(r1=11/2,r2=11/2,h=2);
  translate([0,0,18])cylinder(r1=12/2,r2=12.7/2,h=2.5);
  translate([0,0,20.5])cylinder(r1=12.7/2,r2=12.7/2,h=3.5);
  translate([0,0,24])cylinder(r1=17/2,r2=17.6/2,h=138.8-33.7);
}

module staedler_pen_and_cap() {
  translate([0,0,-33.7+24]) {hull() {
      cylinder(r=16.75/2,h=33.7);
      translate([0,19.7/4,33.7/2])cube([9.75,19.7/2,33.7],center=true); //11
    }
    translate([0,0,33.7])cylinder(r1=16.25/2,r2=17.4/2,h=138.8-33.7);
  }
}

module cable_chain_end(positive=true) {
  difference() { //base
    translate([0,-16,-5])cube([14,20,2],center=true);
    translate([0,-12,-5]) cylinder(r=1.5,h=10,center=true);
    translate([0,-20,-5]) cylinder(r=1.5,h=10,center=true);
  }
  difference() { //main body
    hull() {
      translate([0,0,0])rotate([0,90,0])cylinder(r=5,h=14,center=true);
      translate([0,-25,-4])rotate([0,90,0])cylinder(r=1,h=14,center=true);
    }
    translate([0,-18,0])cube([10,18,10],center=true);
    translate([0,-10,0])cube([7.5,35,10.1],center=true);
    if (positive) {
      difference() { //cut in for link
        difference() {
          translate([0,0,0])rotate([0,90,0])cylinder(r=5.25,h=15,center=true);
          translate([0,0,0])cube([10,14,14],center=true);
        }
        translate([0,0,0])rotate([0,90,0])cylinder(r=2,h=15,center=true);
      }
    } else {
        translate([0,0,0])cube([10,18,14],center=true);
        translate([0,0,0])rotate([0,90,0])cylinder(r=2,h=15,center=true);
    }
  }
}

module cable_chain_link() {
  difference() {
    hull() {
      translate([0,0,0])rotate([0,90,0])cylinder(r=5,h=14,center=true);
      translate([0,14,0])rotate([0,90,0])cylinder(r=5,h=14,center=true);
    }
    translate([0,14,0])rotate([0,90,0])cylinder(r=2,h=15,center=true); //peg cutout
    translate([0,7,0])cube([7.5,28,7.25],center=true);
    translate([0,-2,0])cube([7.5,10,16],center=true);
    translate([0,14,0])cube([10.7,13,7.25],center=true);
    translate([0,15,0])cube([10.7,10,16],center=true);
    difference() { //Link cutout
      difference() {
        translate([0,0,0])rotate([0,90,0])cylinder(r=5.25,h=15,center=true);
        translate([0,0,0])cube([10,14,14],center=true);
      }
      translate([0,0,0])rotate([0,90,0])cylinder(r=2,h=15,center=true);
    }
  }
}

module direct_cable_chain(to_y,to_z,n) {
  theta = atan2(to_z,to_y);
  for (l=[0:n-1]) {
      color("red") translate([0,to_y*l/n,to_z*l/n]) rotate([theta,0,0]) cable_chain_link();
  }
}

module cable_chain_curve(to_z,n,offset=0,link_length=14) {
  r = to_z/2;
  total_length = n*link_length;

  angle = 2*asin(link_length/r/2); //Normal link angle
  curve_length = 180/angle*link_length; //The chain covers segments, not the curve
  overlap_length = (total_length - curve_length - abs(offset))/2;

  //Calculate the number of links in the straight bits and apparate them
  init_length=overlap_length+max(-offset,0);
  init_links = floor(init_length/link_length);
  final_length=overlap_length+max(offset,0);
  final_links = floor(final_length/link_length);
  if (init_links > 0) {
    for (i=[0:init_links-1]) {
      translate([0,i*link_length,0]) cable_chain_link();
    }
  }
  if (final_links > 0) {
    for (i=[1:final_links]) {
      translate([0,-offset +i*link_length,to_z]) rotate([0,0,180])cable_chain_link();
    }
  }

  //Curve
  //The difference between calculated lower length and length covered by links
  curve_offset = init_length - init_links*link_length;
  // use cosine rule to calculate intersection diamond angle of two intersecting circles
  //Distance from link end to centre of curve
  hyp = sqrt(r^2+curve_offset^2);
  inner_a = acos((link_length^2+hyp^2-r^2)/(2*hyp*link_length));
  //Get the initial angle offset of the bend radius
  r_a=asin(r/hyp);
  //subtract.
  init_angle = r_a - inner_a;

  // Initial offset link
  translate([0,init_links*link_length,0])rotate([init_angle,0,0]) cable_chain_link(); 
  //Figure out the inner angle of the offset circle;
  next_a = acos((r^2+hyp^2-link_length^2)/(2*hyp*r));
  //Subtract the offset element
  start_angle = next_a-(90-r_a);
  curve_links = floor((180 - start_angle)/angle)-1;

  translate([0,init_length,r]) {
    for (l=[0:curve_links]) {
      rotate([start_angle+angle*l,0,0]) translate([0,0,-r]) rotate([angle/2,0,0]) cable_chain_link();
    }
  }

  remaining_angle = 180 - start_angle - angle*(curve_links+1);
  final_y = init_length+sin(remaining_angle)*r;
  final_z = r+cos(remaining_angle)*r;
  final_angle = 270-acos((final_z-to_z)/link_length);
  // Final offset link
  translate([0,final_y,final_z])rotate([final_angle,0,0]) cable_chain_link(); 
}

module cable_chain(to_y,to_z,length_in_links,from_invert=false,to_invert=false,link_length=14,max_angle=45) {
  bend_height=2*link_length/(2*sin(max_angle/2));
  color("darkgrey") if (from_invert) {
    rotate([0,180,0])cable_chain_end(positive=false);
  } else {
    cable_chain_end(positive=false);
  }
  length = norm([to_y,to_z]);
  if (to_z*3.1415/2+abs(to_y) >= length_in_links*link_length) {
    //Simply not enough links to cover the distance
    direct_cable_chain(to_y,to_z,length_in_links);
  } else {
    if (to_z < bend_height) {
      direct_cable_chain(to_y,to_z,length/link_length);
    } else {
      color("darkgrey") cable_chain_curve(to_z,length_in_links,offset=-to_y);
    }
    }
  translate([0,to_y,to_z]) rotate([-180,0,180])
  color("darkgrey") if (to_invert) { rotate([0,180,0])cable_chain_end(positive=true);
  } else { cable_chain_end(positive=true);}
}

module microswitch(lever=false) {
  //Full size microswitch, not mini
  difference () {
    union() {
      color("darkred") roundedBox([28,16,10.25],4,true);
      color("grey") translate([9.5,-8.5,0]) cube([9,9,7],center=true);
      color("grey") translate([18,-3,0]) cube([16,3,7],center=true);
      color("grey") translate([18,2.5,0]) cube([16,3,7],center=true);
      color("black") translate([-7,8.5,0]) cube([3,5,5],center=true);
    }
    translate([11.1,5.15,0]) cylinder(r=1.65,h=12,center=true);
    translate([-11.1,-5.15,0]) cylinder(r=1.65,h=12,center=true);
  }
  if (lever) {
    translate([-12,11.5,0]) rotate(-10) cube([30,1,5],center=true);
  }
}

$fn=20;