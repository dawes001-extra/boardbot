include </usr/share/openscad/libraries/MCAD/nuts_and_bolts.scad>
module mod_top() {
  difference() {
    translate([100,73,0]) rotate(180) import("rpi2-top_netfabb.stl");
    translate([50,-44,10]) cube([100,100,20],center=true);
  }
}

module mod_bottom() {
  difference() {
    union() {
      difference() {
        translate([112,72.5,0]) rotate(180) import("rpi2-bottom_netfabb.stl");
        translate([75,-50,10]) cube([150,100,20],center=true);
      }
    translate([65,-2.5,10]) cube([130,5,20],center=true);
    translate([65,-5,10]) cube([130,5,4],center=true);
    }
  translate([8,-3,10]) rotate([90,0,0]) boltHole(4,length=20);
  translate([8,1,10]) rotate([90,0,0]) cylinder(r1=6,r2=4,h=4);
  translate([122,-3,10]) rotate([90,0,0]) boltHole(4,length=20);
  translate([122,1,10]) rotate([90,0,0]) cylinder(r1=6,r2=4,h=4);
  }
}

mod_top();