include </usr/share/openscad/libraries/MCAD/boxes.scad>
include </usr/share/openscad/libraries/MCAD/nuts_and_bolts.scad>
include <vitamins.scad>
include <servo.scad>

module gantry_mockup(y_pos=300) {
  //rails
  translate([60,0,0]) rotate([0,90,0]) base_rail(1290);  
  translate([-60,0,0]) rotate([0,-90,0]) base_rail(1290);  
  //bottom gantry
  translate([65,-36,-627]) rotate([90,0,0]) runner_wheel();
  translate([-65,-36,-627]) rotate([90,0,0]) runner_wheel();
  translate([65,-22,-627]) rotate([90,0,0]) eccentric_nut();
  translate([-65,-22,-627]) rotate([90,0,0]) eccentric_nut();
  translate([-40,9.5,-650]) rotate([90,0,0])idler_wheel();
  translate([40,-1,-650-10]) rotate([90,0,0])idler_wheel();

  translate([0,0,-650]) rotate([0,90,0]) gantry_end(top=false);

  //top gantry
  translate([65,-36,627]) rotate([90,0,0]) runner_wheel();
  translate([-65,-36,627]) rotate([90,0,0]) runner_wheel();
  translate([65,-22,627]) rotate([90,0,0]) eccentric_nut();
  translate([-65,-22,627]) rotate([90,0,0]) eccentric_nut();
  translate([-40,-1,650]) rotate([90,0,0])idler_wheel();
  translate([40,9.5,650+10]) rotate([90,0,0])idler_wheel();

  translate([0,0,650]) rotate([0,-90,0]) gantry_end(top=true);
  
  //belts
  belt([40-6,9.5-4,650+10],[40-6,9.5-4,y_pos]);
  belt([-40+6,-1-4,650],[-40+6,-1-4,y_pos]);
  belt([40-6,-1-4,-650-10],[40-6,-1-4,y_pos]);
  belt([-40+6,9.5-4,-650],[-40+6,9.5-4,y_pos]);
  //toolhead
  translate([0,-5,y_pos]) toolhead_mockup();
  //Y endstop
  translate([5,20,630]) rotate([-90,0,0]) microswitch(true);
}

module toolhead_mockup() {
  translate([70,0,50]) rotate([90,0,0]) runner_wheel();
  translate([-70,0,50]) rotate([90,0,0]) runner_wheel();
  translate([70,12,50]) rotate([90,0,0]) eccentric_nut();
  translate([-70,12,50]) rotate([90,0,0]) eccentric_nut();
  
  translate([70,0,-50]) rotate([90,0,0]) runner_wheel();
  translate([-70,0,-50]) rotate([90,0,0]) runner_wheel();
  translate([70,12,-50]) rotate([90,0,0]) eccentric_nut();
  translate([-70,12,-50]) rotate([90,0,0]) eccentric_nut();
  for (x=[1,-1]) { for (z=[1,-1]) {
  translate([x*55,22,z*35]) mirror([(1-x)/2,0,0]) mirror([0,0,(1-z)/2]) carriage_wheel_holder();
  }}
  simple_carriage();
  simple_toolhead();
}

module simple_toolhead() {
}

module simple_carriage() {
  difference() {
    union() {
      translate([0,22,0])cube([120,4,80],center=true);
      translate([0,21.5,0])cube([85,5,90],center=true);
    }
    for (x=[1,-1]) { for (z=[1,-1]) { 
      translate([x*34,24,z*40])rotate([90,0,0])boltHole(3,length=16);//Aligned to belts
      translate([x*34,21,z*40])rotate([90,0,0])nutHole(3);//Aligned to belts
      hull() { //Screw mount for wheels
        translate([x*56,20,z*35])rotate([90,0,0])cylinder(r=3/2,h=10,center=true);
        translate([x*51,20,z*35])rotate([90,0,0])cylinder(r=3/2,h=10,center=true);
      }
      translate([x*55,24,z*20])rotate([90,0,0])boltHole(3,length=16);
      translate([x*55,24,z*10])rotate([90,0,0])boltHole(3,length=16);
      translate([x*20,24,z*40])rotate([90,0,0])boltHole(3,length=16);
      translate([x*20,21,z*40])rotate([90,0,0])nutHole(3);
    }}
    translate([0,22,0])cube([100,8,60],center=true);
  }
}

module carriage_wheel_holder() {
  difference() {
    union() {
      hull() {
        translate([15,-11,15]) rotate([90,0,0]) cylinder(r=10,h=2,center=true);
        translate([0,0,0]) rotate([90,0,0]) cylinder(r=10,h=4,center=true);
     }
   }
    translate([15,-10,15])
      minkowski() {
        rotate([90,0,0]) eccentric_nut(solid=true);
        cube([0.2,0.2,0.2],center=true);
      }
     translate([7,0.8,-2.8])cube([35,6,16],center=true);
      translate([0,2,0])rotate([90,0,0])boltHole(3,length=10);
      translate([0,-3.5,0])rotate([90,16,0])nutHole(3);
      translate([0,-5,0])rotate([90,16,0])nutHole(3);
      translate([0,-7,0])rotate([90,16,0])nutHole(3);
      rotate([0,45,0]) translate([0,-6,-4])cube([5.6,5,8],center=true);
      translate([15,1,15]) rotate([90,0,0]) cylinder(r=6,h=20,center=true);
   }
}
  

module gantry_end(top=true) {
  //if (top) {
  //  %translate([-20,20,-5]) rotate([0,90,90]) microswitch(true); //Mockup
  //}
  difference() {
    union() {
      translate([-23,-22,0]) cube([18,5,150],center=true); //main
      translate([-9,12,0]) cube([46,5,88],center=true); //outer
      translate([-24,-6,40]) cube([16,32,8],center=true); //A
      translate([-24,-6,-40]) cube([16,32,8],center=true); //B
      translate([-25,0,57]) rotate([0,0,0]) difference() { //truncated rails
          rotate([0,0,180]) outer_rail_captive_nut();
          translate([-12,0,-5]) cube([10,35,20],center=true);
        }
      translate([-25,0,-57]) rotate([180,0,0]) difference() { //truncated rails
          rotate([0,0,180]) outer_rail_captive_nut();
          translate([-12,0,-5]) cube([10,35,20],center=true);
        }
    if (top) {
      translate([-8,4.5,40]) cube([28,11,8],center=true); //A idler
      translate([-3,12,-40]) cube([34,5,8],center=true); //B idler
    } else {
      translate([-3,4.5,40]) cube([34,11,8],center=true); //A idler
    }
    }
    //Cable chain mount holes
    if (!top) {
      for (q=[1,-1]) {
        translate([-23,-26,10*q]) scale(1.1) {
          rotate([-90,0,0]) nutHole(3);
          translate([0,10,0]) rotate([90,0,0]) boltHole(3,length=10);
        }
      }
    }
    //endstop mount
    if (top) translate([-20,12.5,-5]) rotate([0,90,90]) for (q=[1,-1]) {
      hull() {
        translate([11.1*q,3*q,0]) cylinder(r=1.65,h=10,center=true);
        translate([11.1*q,7*q,0]) cylinder(r=1.65,h=10,center=true);
      }
    }
    intersection() { //Decoration
    translate([-9,12,0]) cube([38,10,76],center=true); 
      translate([-9,12,0]) rotate([0,45,0]) union() {
        for (x=top?[-2,-1,1,2]:[-3:2]) { for (z=top?[-2,-1,1,2]:[-3:2]) {
          translate([12*x,0,12*z]) cube([10,10,10],center=true);
        } }
      }
    }
    translate([-23,-22,65])
      minkowski() {
          rotate([90,0,0]) eccentric_nut(solid=true);
          cube([0.2,0.2,0.2],center=true);
      }
    translate([-23,-22,-65])
      minkowski() {
          rotate([90,0,0]) eccentric_nut(solid=true);
          cube([0.2,0.2,0.2],center=true);
      }
    translate([-25,0,42])rotate([0,0,0])boltHole(3,length=16); //side screw
    translate([-25,0,37])rotate([0,0,0])cylinder(r=3.5,h=10,center=true); //side
    translate([-25,0,-42])rotate([0,180,0])boltHole(3,length=16); //side screw
    translate([-25,0,-37])rotate([0,0,0])cylinder(r=3.5,h=10,center=true); //side
    if (top) {
      translate([0,-10,40])rotate([-90,0,0])boltHole(3,length=30); //idler
      translate([10,0,-40])rotate([-90,0,0])boltHole(3,length=20); //idler
    } else {
      translate([10,-10,40])rotate([-90,0,0])boltHole(3,length=30); //idler
      translate([0,15,-40])rotate([90,0,0])boltHole(3,length=20); //idler
    }
  }
}


module outer_rail_captive_nut() {
  difference() {
    union() {
      intersection() {
        rotate([-135,0,0]) translate([-25/2,0,0])cube([25,18,18]);
        translate([0,0,-7])cube([25,22,15],center=true);
      }
      union() {
        for (r=[1,-1]) {
          rotate([45*r,0,0]) translate([0,1*r,-12])cube([25,8,18],center=true);
        }
      }
    }
    for (r=[1,-1]) {
      translate([0,0,5])rotate([45*r,0,0]) cube([28,5,40],center=true); //Rail cutoff      
      translate([0,19*r,0]) cube([28,5,40],center=true); //boundings
    }
    translate([0,0,-16.5]) cube([28,40,5],center=true); //boundings
    translate([0,0,2*sqrt(2)]) minkowski() {
      base_rail(100);
      cube([0.2,0.2,0.2],center=true);
    }
    translate([-9,0,-9.75])cube([25,6,2.75],center=true); //Captive nut slot
    translate([0,0,-9.75])cube([50,2, 2],center=true); //Rear punchout
    translate([0,0,-10])cylinder(r=2,h=20,center=true);
  }
}

module board_captive_bolt(){
  difference() {
    union() {
      translate([9,0,-7]) cube([22,8,18],center=true);
      translate([-7,-3.5,-2]) cube([18,15,8],center=true);
      translate([-6,-3.5,-6])rotate([90,0,0])cylinder(r=5,h=15,center=true);
      translate([-2,-3.5,-7]) cube([8,15,18],center=true);
    }
    translate([15,0,-9]) {
      rotate([90,0,0])cylinder(r=5,h=30,center=true);
      for (q=[0:20:140]) {
        rotate([0,q,0]) translate([10,0,0])cube([20,30,10],center=true);
      }
    }
    translate([-6,0,-6])rotate([90,0,0])cylinder(r=2.1,h=30,center=true);
    translate([-6,-6,-6])rotate([90,0,0]) cylinder(r=4,h=10,center=true);

    translate([-1494/2,0,-1195/2]) board();
    translate([10,0,0])scale(1.15)boltHole(3,length=12);
    #translate([10,0,0])boltHole(3,length=12);
    translate([23,0,0])cube([30,2.75,10],center=true);
    translate([23,0,-7])cube([30,4,10],center=true);
    translate([23,0,-1])cube([30,5.75,3],center=true);
  }
}

module end_cap_idler_holder() {
  difference() {
    union() {
      intersection() {
        rotate([-135,0,0]) translate([-10,-11,-1]) cube([15,32,22]);
        translate([0,0,-4])cube([30,30,24],center=true);
      }
      translate([0,10,16.5]) cube([10,10,61],center=true);
      intersection() {
        translate([0,18.5,37]) cube([10,7,20],center=true);
        translate([0,12,41]) rotate([45,0,0]) cube([10,25,20],center=true);
      }
    }
    translate([-50,0,0])
      minkowski() {
        base_rail(100);
        cube([0.2,0.2,0.2],center=true);
      }
      
    for (z=[0,30]) { //Strengthening brackets
      translate([0,10,z]) rotate([0,-90,0]) scale(1.1) {hull() {
          translate([0,0,0]) nutHole(3);
          translate([0,0,10]) nutHole(3);
        }
        translate([0,0,-10])cylinder(r=1.5,h=40);
      }
    }
    translate([0,3,42])rotate([-90,0,0])boltHole(3,length=40);
    translate([-3,0,-14])rotate([0,0,0])boltHole(3,length=12); //Grab bolt
    translate([-9,0,-9.75])cube([25,5.6,2.75],center=true); //Captive nut slot
  }
}

module end_cap_vertical_strengthener() {
  difference() {
    union() {
      translate([0,10,5]) cube([10,10,60],center=true);
      intersection() {
        translate([-2,10,-25]) rotate([0,0,-45]) cube([23,23,20]);
        translate([2,10,-16]) cube([30,30,30],center=true);
      }
    }
    rotate([0,-90,0]) translate([-60,10,0])
      minkowski() {
        base_rail(100);
        cube([0.2,0.2,0.2],center=true);
      }
      rotate([0,-90,0]) translate([30,10,-4])boltHole(3,length=20);
      rotate([0,-90,0]) translate([0,10,-4])boltHole(3,length=20);
      translate([15,10,-15])rotate([0,-90,0])boltHole(3,length=12);
      translate([9,10,-9.75])cube([2.75,5.6,25],center=true); //Captive nut slot
  }
}

module end_stepper_holder_back(upper=true){
  //%translate([55,upper?10:0,upper?32.5:-32.5]) rotate([0,upper?-90:90,-90]) microswitch(true); //mockup
  //%if (!upper){translate([70-25,1,28]) rotate([0,0,-90])cable_chain(100,40,15);} //mockup
  mirror([0,0,upper?0:1]) difference() {
    union() {
        intersection() {
          rotate([-135,0,0]) translate([-10,upper?-11:-15,upper?-1:-8]) cube([50,upper?50:70,22]);
          translate([15,upper?-1.5:-6.5,upper?-20:-10])cube([50,upper?33:43,upper?55:70],center=true);
        }
        translate([15,10,25]) cube([50,10,71],center=true);
        translate([15,upper?-15:-25,upper?-42:-52]) cube([50,6,upper?50:30],center=true);
    if (!upper) {
        translate([27.5,1,-16]) cube([25,20,12],center=true); //Chain mount      
        difference() {
          union() { //Endstop mount
            translate([15,-8,46]) cube([50,40,10],center=true);
            translate([27.5,-8,26]) cube([25,40,50],center=true);
          }
          translate([15,-40,30]) rotate(45) cube([50,50,70],center=true);
          translate([27.5,-23,11]) cube([30,30,60],center=true);
        }
    }
    }
    if (!upper) {
      translate([28.45,2,-29]) cube([27,22,14],center=true); //Chain mount cutout
      translate([25,1,-25]) rotate([0,0,0]) boltHole(3,length=12);; //Chain mount bolts
      translate([25,10,-18])cube([5.6,25,2.75],center=true); //Captive nut slot
      translate([33,1,-25]) rotate([0,0,0]) boltHole(3,length=12);; //Chain mount bolts
      translate([33,10,-18])cube([5.6,25,2.75],center=true); //Captive nut slot
    }
    translate([25,upper?0:-10,-14]) { //Bolt
      boltHole(3,length=12);
      rotate([0,180,0])cylinder(r=4,h=20);
    }
    translate([34,upper?0:-10,-9.75])cube([25,5.6,2.75],center=true); //Captive nut slot
    translate([50,upper?0:-10,0]) //Rail
      minkowski() {
        base_rail(100);
        cube([0.2,0.2,0.2],center=true);
      }
  translate([35,upper?0:-10,35]) rotate([-90,0,0]) boltHole(3,length=20);
  translate([46,upper?10:0,35])cube([30,2.75,5.6],center=true); //Captive nut slot
  translate([35,upper?0:-10,15]) rotate([-90,0,0]) boltHole(3,length=20);
  translate([46,upper?10:0,15])cube([30,2.75,5.6],center=true); //Captive nut slot
  translate([15,17,29]) rotate([90,0,0]) cylinder(r=12,h=10,center=true); //Stepper mount spaces
    translate([15,38,29]) {
      for (x = [1,-1]) {for (y = [1,-1]) {
        translate([18*x,0,25*y]) rotate([90,0,0])boltHole(3,length=50);
        translate([18*x,-26,25*y]) rotate([90,0,0])nutHole(3);
        translate([27*x,-27.2,25*y]) cube([20,2.8,6],center=true);
      }}
    }
  }
}

module end_endstop_plate(){
  difference() {
    cube([35,2,40]);
    translate([-1,-1,-1])cube([13,4,11]);
//    translate([0,0,0])cube([0,0,0]);
   translate([5,-1,35]) rotate([-90,0,0])boltHole(3,length=20);
   translate([5,-1,15]) rotate([-90,0,0])boltHole(3,length=20);
    //Stepper mount
    translate([25,0,18]) rotate([0,-90,90]) for (q=[1,-1]) {
      hull() {
        translate([11.1*q,3*q,0]) cylinder(r=1.65,h=10,center=true);
        translate([11.1*q,7*q,0]) cylinder(r=1.65,h=10,center=true);
      }
    }
  }
}

module end_stepper_holder_plate(){
  //This needs splitting into two objects.
  difference() {
    union() {
      translate([0,1,0]) cube([45,3,60],center=true);
      translate([32,-0.5,2]) { difference() { cube([26,6,20],center=true);
      translate([-13,-3,0]) rotate([0,0,45])cube([5,5,50],center=true); }
      }
    }
  translate([0,0,0]) rotate([90,0,0]) cylinder(r=12,h=10,center=true);
    translate([0,1,0]) {
      for (x = [1,-1]) {for (y = [1,-1]) {
        translate([15.5*x,0,15.5*y]) rotate([-90,0,0])#boltHole(3,length=10);
      }}
    }
  translate([35,-12,2]) rotate([-90,0,0])#boltHole(3,length=18);
    translate([0,28,0]) {
      for (x = [1,-1]) {for (y = [1,-1]) {
        translate([18*x,-25,25*y]) rotate([90,0,0])#boltHole(3,length=30);
      }}
    }
  }
}

module mockup(x_pos=800,y_pos=300) {
  board();
//Top rail
  translate([70,0,618]) rotate([0,0,0]) base_rail(1700);
  translate([757,0,618-2*sqrt(2)]) outer_rail_captive_nut();
  translate([-757,0,618-2*sqrt(2)]) outer_rail_captive_nut();
  translate([1494/2,0,1195/2]) board_captive_bolt();
  translate([-1494/2,0,1195/2]) mirror([1,0,0])board_captive_bolt();

//Bottom rail
  translate([70,0,-618]) rotate([180,0,0]) base_rail(1700);
  translate([757,0,-618+2*sqrt(2)]) rotate([180,0,0])outer_rail_captive_nut();
  translate([-757,0,-618+2*sqrt(2)]) rotate([180,0,0])outer_rail_captive_nut();
  translate([1494/2,0,-1195/2]) mirror([0,0,1])board_captive_bolt();
  translate([-1494/2,0,-1195/2]) rotate([0,180,0])board_captive_bolt();

//End idlers and holders
  translate([920,0,618]) end_cap_idler_holder();
  translate([920,31,660]) rotate([90,0,0])idler_wheel();
  translate([920,33,660]) rotate([-90,0,0])idler_wheel();
  translate([920,0,-618]) mirror([0,0,1])end_cap_idler_holder();
  translate([920,31,-660]) rotate([90,0,0])idler_wheel();
  translate([920,33,-660]) rotate([-90,0,0])idler_wheel();
//Extra strengthener
  translate([930,0,618]) end_cap_vertical_strengthener();
  translate([930,0,-618]) mirror([0,0,1])end_cap_vertical_strengthener();
  translate([930,10,0]) rotate([0,90,180]) base_rail(1215);

//Steppers and holders
  translate([-795,35,647]) rotate([90,0,0]) {
    stepper();
    translate([0,0,21]) rotate([180,0,0]) drive_wheel();
    translate([35,2,4]) idler_wheel();
  translate([-15,-29,35]) rotate([-90,0,0]) end_stepper_holder_back(true);
  translate([0,0,0]) rotate([-90,0,0]) end_stepper_holder_plate();
  }

//X endstops
  translate([-780,0,-667.5]) end_endstop_plate();
  translate([-780,0,667.5]) mirror([0,0,1]) end_endstop_plate();
  translate([-755,10,-650]) rotate([0,90,-90]) microswitch(true);
  translate([-755,10,650]) rotate([0,-90,-90]) microswitch(true);

  translate([-795,45,-647]) mirror([0,0,1]) rotate([90,0,0]) {
    stepper();
    translate([0,0,21]) rotate([180,0,0]) drive_wheel();
    translate([35,2,4]) idler_wheel();
  translate([-15,-29,35]) rotate([-90,0,0]) mirror([0,0,1]) end_stepper_holder_back(false);
  translate([0,0,0]) rotate([-90,0,0]) end_stepper_holder_plate();
  }

  //Belts for visualisation
  belt([920+6,31-4,660],[920+6,31-4,-660]);
  belt([920+6,33+4,660],[920+6,33+4,-660]);

  belt([920,33+4,-660-6],[-795,33+4,-660-6]);
  belt([920,31-4,660+6],[-795,33-4,660+6]);
//belts to/from gantry
  belt([920,33+4,660+6],[x_pos+40,33+4,660+6]);
  belt([-760,33+4,-650-6],[x_pos-40,33+4,-650-6]);

  belt([920,31-4,-660-6],[x_pos+40,31-4,-660-6]);
  belt([-760,31-4,650+6],[x_pos-40,31-4,650+6]);
  
  //Cable chain
  translate([-740-25,11,-590]) rotate([0,0,-90])cable_chain(x_pos+740+25+20,38,120);
//Gantry elements
  translate([x_pos,32,0]) gantry_mockup(y_pos);
}

module pen_sleeve() {
  translate([0,0,0])difference() {
    union() {
      translate([0,0,47.5]) cylinder(r1=12,r2=10,h=5,center=true);
      translate([0,0,40]) cylinder(r=12,h=10,center=true);
      translate([0,0,32.5]) cylinder(r1=10,r2=12,h=5,center=true);
    }
    #staedler_pen();
    translate([0,0,40])
      for (i=[0:11]) {
        rotate(360/12*i)translate([12,0,0]) scale([0.5,1,1]) sphere(r=3);
    }
  }
}

module pen_grip_test(left=true) {
  difference() {
    union() {
      difference() {
        cylinder(h=6,r=14,center=true);
        cylinder(h=8,r=12,center=true);
      }
      for (i=left?[7:11]:[0:5]) {
        rotate(360/12*i)translate([12,0,0]) scale([0.4,0.9,0.9]) sphere(r=3);
      }
    }
    translate([0,left?5:-5,0])cube([30,18,8],center=true);
    difference() {
      cylinder(h=8,r=16,center=true);
      cylinder(h=10,r=14,center=true);
    }
  }
  //Arms and hinge
  difference() {
    translate([-30,0,left?-1.5:1.5])rotate(left?-18:18)translate([0,0,0])cube([50,5,3],center=true);
    translate([-30,0,1]) cylinder(r=3.2/2,h=20,center=true);
        cylinder(h=8,r=12,center=true);
  }
  //Spring holder
  if (left) {
  difference() {
  translate([-20,-8,0])cube([5,10,6],center=true);
  translate([-20,left?-11:11,-3]) boltHole(3,length=10);
  }} else {
  difference() {
  translate([-20,8,0])cube([5,10,6],center=true);
  translate([-20,left?-11:11,-3]) boltHole(3,length=10);
  }}
}

module servo_flange(open=false) {
  rotate(open?90:0) {
    hull() {
    translate([10,0,5])cylinder(r=2,h=10,center=true);
    translate([0,0,5])cylinder(r=4,h=10,center=true);
    translate([-10,0,5])cylinder(r=2,h=10,center=true);
    }
  }
}

module toolchanger_mockup(open=false) {
//  x=open?1:2;
  translate([0,0,40]) {
  translate([-30,0,0])rotate(open?-15:0)translate([30,0,0])pen_grip_test(true);
  translate([-30,0,0])rotate(open?15:0)translate([30,0,0])pen_grip_test(false);
  translate([open?-23:-20,0,5]) rotate([90,0,0])tension_spring(open?1.5:0.5);
  }
  pen_sleeve();
  translate([-50,0,40])servo();
  translate([-50,0,40])servo_flange(open);
}

//mockup(0,-580);//Limits are [[820,-690],[580,-580]]
//toolchanger_mockup(false);
//rotate([0,90,0])end_stepper_holder_back(false);
//rotate([0,-90,0])gantry_end(false);
//rotate([90,0,0])mirror([1,0,0])carriage_wheel_holder();
//rotate([-90,0,0])simple_toolhead();
gantry_end(false);

$fn=20;